package com.aus.amaysim.openqa.executor;

import org.openqa.selenium.By;

import com.aus.amaysim.openqa.command.ClickCommand;
import com.aus.amaysim.openqa.command.DisplayCommand;
import com.aus.amaysim.openqa.command.EnabledCommand;
import com.aus.amaysim.openqa.handler.NavigateElement;

/**
 * @author rguballo
 *
 */
public class Button implements ClickCommand, EnabledCommand, DisplayCommand {

    private final By locator;

	public Button(final By locator) {
		this.locator = locator;
	}

	@Override
	public void CLICK() {
		NavigateElement.executeInstance().clickElement(locator);
	}

	@Override
	public void ACTION_CLICK() {
		NavigateElement.executeInstance().actionClickElement(locator);
		
	}

	@Override
	public void MULTIPLE_CLICK(int times) {
		NavigateElement.executeInstance().multipleClickElement(locator, times);
	}

	@Override
	public void VERIFY_IF_ENABLED() {
		NavigateElement.executeInstance().verifyEnabledElement(locator);
	}

	@Override
	public void VERIFY_IF_DISABLED() {
		NavigateElement.executeInstance().verifyDisabledElement(locator);
	}

	@Override
	public void VERIFY_IF_DISPLAYED() {
		NavigateElement.executeInstance().verifyDisplayedElement(locator);
	}

	@Override
	public void VERIFY_IF_NOT_DISPLAYED() {
		NavigateElement.executeInstance().verifyNotDisplayedElement(locator);
	}
}
