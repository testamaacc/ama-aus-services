package com.aus.amaysim.openqa.executor;

import org.openqa.selenium.By;

import com.aus.amaysim.openqa.command.DisplayCommand;
import com.aus.amaysim.openqa.command.DropdownCommand;
import com.aus.amaysim.openqa.handler.NavigateElement;

public class Dropdown implements DropdownCommand, DisplayCommand {
	
    private final By locator;

	public Dropdown(final By locator) {
		this.locator = locator;
	}

	@Override
	public void SELECT_OPTION_BY_TEXT(String value) {
		NavigateElement.executeInstance().selectDropdownOptionByText(locator, value);
		
	}

	@Override
	public void SELECT_OPTION_BY_VALUE(String value) {
		NavigateElement.executeInstance().selectDropdownOptionByValue(locator, value);
		
	}

	@Override
	public void SELECT_OPTION_BY_INDEX(int value) {
		NavigateElement.executeInstance().selectDropdownOptionByIndex(locator, value);
		
	}

	@Override
	public void VERIFY_IF_DISPLAYED() {
		NavigateElement.executeInstance().verifyDisplayedElement(locator);
		
	}

	@Override
	public void VERIFY_IF_NOT_DISPLAYED() {
		NavigateElement.executeInstance().verifyNotDisplayedElement(locator);
		
	}
}
