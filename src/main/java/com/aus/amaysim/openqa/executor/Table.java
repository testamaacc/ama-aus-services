package com.aus.amaysim.openqa.executor;

import org.openqa.selenium.By;

import com.aus.amaysim.openqa.command.CountCommand;
import com.aus.amaysim.openqa.command.DisplayCommand;
import com.aus.amaysim.openqa.handler.NavigateElement;

/**
 * @author rguballo
 *
 */
public class Table implements DisplayCommand, CountCommand {

    private final By locator;

	public Table(final By locator) {
		this.locator = locator;
	}

	@Override
	public void VERIFY_IF_DISPLAYED() {
		NavigateElement.executeInstance().verifyDisplayedElement(locator);
	}

	@Override
	public void VERIFY_IF_NOT_DISPLAYED() {
		NavigateElement.executeInstance().verifyNotDisplayedElement(locator);
	}

	@Override
	public void VERIFY_COUNT_LIST(int count) {
		NavigateElement.executeInstance().verifyCountTableList(locator, count);
		
	}
}
