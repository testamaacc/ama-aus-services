package com.aus.amaysim.openqa.core;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.rules.TestName;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.aus.amaysim.openqa.handler.NavigateElement;
import com.aus.amaysim.utilities.constant.DriverInfo;
import com.aus.amaysim.utilities.constant.DriverPath;
import com.aus.amaysim.utilities.dataprocess.GetData;
import com.aus.amaysim.utilities.report.ExtentManager;
import com.aus.amaysim.utilities.report.TestInfo;
import com.relevantcodes.extentreports.ExtentReports;

/**
 * @author rguballo
 *
 */
public class BaseTest {
	
	protected static WebDriver driver;
	protected static ExtentReports extent;
	protected static WebDriverWait wait;
	@Rule public TestName name = new TestName();
    
	@BeforeClass
	public static void startReport() {
		
		//--> Creating report
		extent = ExtentManager.getInstance();
		
		//--> Launching chrome browser
		System. setProperty(DriverInfo.CHROME_DRIVER, DriverPath.CHROME_DRIVER_PATH);
		driver = new ChromeDriver();
		wait = new WebDriverWait(driver, 10);
	}
	
	@Before
	public void openBrowser() {
		
		TestInfo.startTestNameDetails("Test Method: " + name.getMethodName(), "Selenium Automation Tester");
		
		//--> Loading of url
		NavigateElement.executeInstance().runURL(GetData.executeInstance().url("amaysim_url"));
	}
	
	@AfterClass
	public static void produceReport() {
		extent.flush();
	}
}
