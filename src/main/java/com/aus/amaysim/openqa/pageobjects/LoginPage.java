package com.aus.amaysim.openqa.pageobjects;

import org.openqa.selenium.By;

import com.aus.amaysim.openqa.executor.Button;
import com.aus.amaysim.openqa.executor.LinkButton;
import com.aus.amaysim.openqa.executor.TextBox;

/**
 * @author rguballo
 *
 */
public class LoginPage {

	public static class LoginForm {
		public static final LinkButton login_Account = new LinkButton(By.xpath("*//a[contains(@data-js, 'login')]"));
		
		public static final TextBox username = new TextBox(By.id("username"));
		public static final TextBox password = new TextBox(By.id("password"));
		
		public static final Button login = new Button(By.xpath("*//button[contains(text(), 'login')]"));
	}
}
