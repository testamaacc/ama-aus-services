package com.aus.amaysim.openqa.pageobjects;

import org.openqa.selenium.By;

import com.aus.amaysim.openqa.executor.Tab;

/**
 * @author rguballo
 *
 */
public class HeaderBar {

	public static class TabSelection {
		public static final Tab myAccount = new Tab(By.xpath("*//div[contains(@id, 'block-useraccountmenu')]/descendant::span[contains(text(), 'My account')]"));
		public static final Tab Logout = new Tab(By.xpath("*//a[contains(text(), 'Logout')]"));
	}
}
