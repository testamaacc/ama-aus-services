package com.aus.amaysim.openqa.pageobjects;

import org.openqa.selenium.By;

import com.aus.amaysim.openqa.executor.Accordion;
import com.aus.amaysim.openqa.executor.Button;
import com.aus.amaysim.openqa.executor.Dropdown;
import com.aus.amaysim.openqa.executor.LinkButton;
import com.aus.amaysim.openqa.executor.Tab;
import com.aus.amaysim.openqa.executor.Table;
import com.aus.amaysim.openqa.executor.Text;
import com.aus.amaysim.openqa.executor.TextBox;

/**
 * @author rguballo
 *
 */
public class HomePage {

	public static class Header {
		public static final Button logout = new Button(By.xpath("*//div[contains(@class, 'logout-section-link')]"));

	}
	
	public static class MenuContainer {
		public static final Text service_Primary_Info = new Text(By.xpath("*//div[contains(@class, 'service-primary-heading')]"));
		public static final Text service_Secondary_Info = new Text(By.xpath("*//div[contains(@class, 'service-secondary-heading')]"));
		public static final LinkButton myDashboard = new LinkButton(By.xpath("*//a[contains(@href, 'services')]"));

		public static final Tab Plan = new Tab(By.xpath("*//li/descendant::span[contains(text(), 'Plan')]"));
		public static final Tab Payments = new Tab(By.xpath("*//li/descendant::span[contains(text(), 'Payments')]"));
		public static final Tab PaymentMethod = new Tab(By.xpath("*//li/a[contains(text(), 'Payment method')]"));
		public static final Tab PersonalDetails = new Tab(By.xpath("*//li/a[contains(text(), 'Personal details')]"));
		public static final Tab MobileHelp = new Tab(By.xpath("*//li/descendant::span[contains(text(), 'Mobile help')]"));
	}
	
	public static class PlanContent {
		public static final Text plan = new Text(By.xpath("*//div[contains(@class ,'ama-page-header')]"));
		public static final Accordion notifications = new Accordion(By.xpath("*//a[contains(@href, 'msg_content')]"));
		
	}
	
	public static class PaymentContent {
		public static final Text payments = new Text(By.xpath("*//div[contains(@class ,'ama-page-header')]"));
		public static final Table paymentTransactionsTable = new Table(By.xpath("*//div[contains(@class ,'page-container')]/ul[contains(@id, 'invoice_container')]"));
		public static final Table paymentTransactionsTable_List = new Table(By.xpath("*//div[contains(@class ,'page-container')]/descendant::li[contains(@class, 'row ama-section-block')]"));
		public static final Button showMore = new Button(By.id("get_more_invoices"));

	}
	
	public static class PaymentMethodContent {
		public static final Text paymentMethod = new Text(By.xpath("*//div[contains(@class ,'ama-page-header')]"));
		
		public static final LinkButton change = new LinkButton(By.xpath("*//a[contains(@class, 'edit_payment')]"));
		public static final Dropdown changeTo_NewPaymentMethod = new Dropdown(By.id("payment_method_option"));
		public static final Button save_BPMethod = new Button(By.xpath("*//form[contains(@id, 'change_to_bpay_form')]/descendant::input[contains(@name, 'commit')]"));
		public static final LinkButton cancel_BPMethod = new LinkButton(By.xpath("*//form[contains(@id, 'change_to_bpay_form')]/descendant::a[contains(@class, 'cancel_change_payment_btn')]"));	
		
		public static final Table simInfoTable = new Table(By.xpath("*//div[contains(@class, 'row ama-sims')]"));

	}
	
	public static class PersonalDetailsContent {
		public static final Text personalDetails = new Text(By.xpath("*//div[contains(@class ,'ama-page-header')]"));
		
		public static final Text personalInfo_SectionHeader = new Text(By.xpath("*//h2[contains(text(), 'Personal Info')]"));
		public static final Text fullName_Value = new Text(By.xpath("*//div[contains(@id, 'personal_info_block')]/descendant::div[6]"));
		public static final Text dataofBirth_Value = new Text(By.xpath("*//div[contains(@id, 'personal_info_block')]/descendant::div[9]"));
		
		public static final Text contactInfo_SectionHeader = new Text(By.xpath("*//h2[contains(text(), 'Contact Info')]"));
		public static final LinkButton editContact = new LinkButton(By.xpath("*//a[contains(@href, 'edit_contacts')]"));
		public static final TextBox phoneNumber_Value = new TextBox(By.xpath("*//div[contains(@id, 'contacts_block')]/descendant::div[7]"));
		public static final TextBox emailAddress_Value = new TextBox(By.xpath("*//div[contains(@id, 'contacts_block')]/descendant::div[10]"));
		public static final TextBox phoneNumber_Textbox = new TextBox(By.id("my_amaysim2_client_daytime_phone"));
		public static final TextBox emailAddress_Textbox = new TextBox(By.id("my_amaysim2_client_email"));
		
		public static final Button saveContact = new Button(By.xpath("*//form[contains(@class, 'edit_my_amaysim')]/descendant::input[contains(@value, 'Save')]"));
		public static final Button cancel_EditContact = new Button(By.id("show_contacts"));
		public static final Text phoneNumber_ErrorMessage = new Text(By.xpath("*//div[contains(@class, 'daytime_phone')]/span[contains(@class, 'error')]"));
		public static final Text emailAddress_ErrorMessage = new Text(By.xpath("*//div[contains(@class, 'client_email')]/span[contains(@class, 'error')]"));
		
		public static final Text myPassword_SectionHeader = new Text(By.xpath("*//h2[contains(text(), 'My amaysim password')]"));
		public static final Text myAddress_SectionHeader = new Text(By.xpath("*//h2[contains(text(), 'My addresses')]"));
		public static final Text residential_Value = new Text(By.xpath("*//div[contains(@id, 'addresses_block')]/descendant::div[10]"));
		public static final LinkButton editResidential = new LinkButton(By.id("edit_residential"));
		public static final TextBox editResidentialAddress = new TextBox(By.id("edit_residential_address"));
		public static final LinkButton cancel_EditResidential = new LinkButton(By.id("cancel_edit_residential"));
		
		public static final Text delivery_Value = new Text(By.xpath("*//div[contains(@id, 'addresses_block')]/descendant::div[17]"));
		public static final LinkButton editDelivery = new LinkButton(By.id("edit_delivery"));
		public static final TextBox editDeliveryAddress = new TextBox(By.id("edit_delivery_address"));
		public static final LinkButton cancel_EditDelivery = new LinkButton(By.id("cancel_edit_delivery"));

	}
	
	public static class SuccessfulModal {
		public static final Text success_Header = new Text(By.xpath("*//div[contains(@style, 'block')]/h1[contains(@class, 'popup-success white')]"));
		
		public static final Button closeRevealModal = new Button(By.xpath("*//div[contains(@style, 'block')]/a[contains(@class, 'close-reveal-modal')]"));

	}
}
