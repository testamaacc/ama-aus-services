package com.aus.amaysim.openqa.command;

/**
 * @author rguballo
 *
 */
public interface DropdownCommand {
	
	void SELECT_OPTION_BY_TEXT(String value);
	
	void SELECT_OPTION_BY_VALUE(String value);
	
	void SELECT_OPTION_BY_INDEX(int value);
	
}
