package com.aus.amaysim.openqa.command;

/**
 * @author rguballo
 *
 */
public interface CountCommand {

	void VERIFY_COUNT_LIST(int count);
	
}
