package com.aus.amaysim.openqa.command;

/**
 * @author rguballo
 *
 */
public interface DisplayCommand {

	void VERIFY_IF_DISPLAYED();
	
	void VERIFY_IF_NOT_DISPLAYED();
	
}
