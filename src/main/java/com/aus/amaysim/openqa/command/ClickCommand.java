package com.aus.amaysim.openqa.command;

/**
 * @author rguballo
 *
 */
public interface ClickCommand {

	void CLICK();

	void ACTION_CLICK();
	
	void MULTIPLE_CLICK(int times);
}
