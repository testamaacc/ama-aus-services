package com.aus.amaysim.openqa.command;

/**
 * @author rguballo
 *
 */
public interface SelectCommand {

	void SELECT_TILE(String value);
	
}
