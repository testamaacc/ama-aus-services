package com.aus.amaysim.openqa.command;

/**
 * @author rguballo
 *
 */
public interface TextBoxCommand {

	void INSERT_TEXT(String text);
	
	void CLEAR_TEXT();
	
	void VERIFY_TEXT(String text);
	
}
