package com.aus.amaysim.utilities.helper;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

/**
 * @author rguballo
 *
 */
@SuppressWarnings("deprecation")
public class ExtractExcelData {

	private static InputStream input = null;
	private static Workbook wb = null;
	
	public static String getData(String excelPath, String excelSheet, String data) {
		String dir = StringQaHelper.append(System.getProperty("user.dir"), "\\data\\", excelPath);
		try {
			InputStream input = new FileInputStream(dir);
			Workbook wb = WorkbookFactory.create(input);

			Sheet sheet = wb.getSheet(excelSheet);
			int index = getRowIndex(sheet, data);
			if (index >= 0) {

				List<List<String>> str = constructTable(sheet, index);
				List<String> result = str.get(0);
				
				return result.get(0).toString();
				 
			}
		} catch (Exception e) {
			e.getMessage();
		} finally {
			try {
				if (input != null) {
					
					input.close();
					wb.close();
				}
			} catch (IOException io) {
				io.getMessage();
			}
		}
		return null;
	}
	
	private static int getRowIndex(Sheet sheet, String name) {
		for (int i = sheet.getFirstRowNum(); i <= sheet.getLastRowNum(); i++) {
			Row row = sheet.getRow(i);
			if (row != null && row.getCell(0) != null && row.getCell(0).getStringCellValue().equalsIgnoreCase(name)) {
				return i;
			}
		}
		return -1;
	}
	
	private static List<String> getRowValues(Row row, int start, int end) {
		List<String> res = new ArrayList<>();
		for (int i = start; i <= end; i++) {
			res.add(getCellValue(row.getCell(i)));
		}
		return res;
	}
	
	private static String getCellValue(Cell cell) {
		String cellValue = "";
		if (cell.getCellType() == HSSFCell.CELL_TYPE_NUMERIC) {
			if (HSSFDateUtil.isCellDateFormatted(cell)) {
				SimpleDateFormat df = new SimpleDateFormat("yyyy-mm-dd");
				cellValue = df.format(cell.getDateCellValue());
			} else {
				double numericValue = cell.getNumericCellValue();
				cellValue = isInteger(numericValue) ? Integer.toString((int) numericValue)
						: Double.toString(numericValue);
			}
		} else {
			cellValue = cell.getStringCellValue();
		}
		return cellValue;
	}
	
	private static List<List<String>> constructTable(Sheet sheet, int rowIndex) {
		List<List<String>> table = new ArrayList<>();
		Row firstRow = sheet.getRow(rowIndex);
		int numberOfRows = firstRow.getLastCellNum() - 1;
		table.add(getRowValues(firstRow, firstRow.getFirstCellNum() + 1, numberOfRows));
		for (int i = rowIndex + 1; i <= sheet.getLastRowNum(); i++) {
			Row currentRow = sheet.getRow(i);
			if (currentRow == null || currentRow.getCell(1) == null || currentRow.getCell(0) != null) {
				
				break;
			}
			table.add(getRowValues(currentRow, currentRow.getFirstCellNum(), numberOfRows));
		}
		return table;
	}
	
	private static boolean isInteger(double val) {
		return val % 1 == 0;
	}
}
