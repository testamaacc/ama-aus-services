package com.aus.amaysim.utilities.helper;

/**
 * @author rguballo
 *
 */
public class StringQaHelper {
	
	/**
	 * Append multiple declared Strings
	 * 
	 * @param params
	 * @return - String result
	 */
	public static String append(String... params) {
		StringBuilder result = new StringBuilder();
		
		for(String param : params) {
			result.append(param);
		}
		
		return result.toString().trim();
	}

	/**
	 * Append multiple declared Strings with space
	 * 
	 * @param params
	 * @return - String result
	 */
	public static String appendWithSpace(String... params) {
		StringBuilder result = new StringBuilder();
		
		for(String param : params) {
			result.append(param + " ");
		}
		
		return result.toString().trim();
	}
	
	/**
	 * Retrieve all white spaces from declared String
	 * 
	 * @param s
	 * @return - false
	 */
	public static boolean containsWhiteSpace(String s) {
		for (char c : s.toCharArray()) {
			if (Character.isWhitespace(c)) {
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * This method will extract all digits in the string.
	 * 
	 * @param stringToTrim
	 * @return Digits in string format
	 */
	public static String extractDigitsInString(String stringToTrim) {
		StringBuilder str = new StringBuilder();
		for (int i = 0; i < stringToTrim.length(); i++) {
			char c = stringToTrim.charAt(i);
			if (Character.isDigit(c)) {
				str.append(c);
			} // End if
		} // End for
		return str.toString();
	}

}
