package com.aus.amaysim.utilities.report;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;

import org.apache.log4j.Logger;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.ImeActivationFailedException;
import org.openqa.selenium.ImeNotAvailableException;
import org.openqa.selenium.InvalidCookieDomainException;
import org.openqa.selenium.InvalidElementStateException;
import org.openqa.selenium.InvalidSelectorException;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.NoSuchFrameException;
import org.openqa.selenium.NoSuchWindowException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.UnableToSetCookieException;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.interactions.MoveTargetOutOfBoundsException;
import org.openqa.selenium.support.ui.UnexpectedTagNameException;

import com.aus.amaysim.utilities.helper.DateFormatUtils;
import com.aus.amaysim.utilities.helper.StringQaHelper;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

/**
 * @author rguballo
 *
 */
public class ExtentManager {
	
	private static final Logger LOGGER = Logger.getLogger(ExtentManager.class);
	
	// --> Data member
	public static ExtentReports extent;
	public static File extentReportFile;
	public static String osName;

	/**
	 * This method setup ExtentReports
	 * 
	 * @return
	 */
	@SuppressWarnings("deprecation")
	public static ExtentReports getInstance() {
		
		extentReportFile = new File(StringQaHelper.append("reports/ATAF_HTMLReport_", DateFormatUtils.dateFormatWithTime(), ".html"));
		
		String getExtentReportFile = null;
		
		try {
			getExtentReportFile = extentReportFile.getCanonicalPath();
		} catch (IOException e) {
			// --> Print error details in console.
			LOGGER.error(e.getMessage().toString());
		}
		
		if (extent == null) {

			extent = new ExtentReports(getExtentReportFile, true);

				extent.config()
						.documentTitle("tmnph")
						.reportName("ATAF")
						.reportHeadline("Report Automation Test Result");

			extent.addSystemInfo("Selenium Version", "3.0 Beta")
					.addSystemInfo("Environment", "tmnph Production Server");
			
		} //--> End of if condition

		return extent;

	}

	/**
	 * This method is for changing the font style and label of the test status
	 * 
	 * @param label
	 * @return StatusLabel
	 */

	public static String statusLableStyle(ReportStatusLabel label) {

		String statusLabelStyle = "";
		switch (label) {

		case PASSED:
			statusLabelStyle = "<span class='label success'>Passed</span>";
			break;

		case FAILED:
			statusLabelStyle = "<span class='label fail'>Failed</span>";
			break;

		case ERROR:
			statusLabelStyle = "<span class='label error'>Error</span>";
			break;

		case WARNING:
			statusLabelStyle = "<span class='label warning'>Warning</span>";
			break;

		case FATAL:
			statusLabelStyle = "<span class='label fatal'>Fatal</span>";
			break;

		case INFO:
			statusLabelStyle = "<span class='label info'>Info</span>";
			;
			break;
		}
		return statusLabelStyle;
	}

	/**
	 * <h1>webDriverExceptionHandler</h1>
	 * <ul>
	 * This method will handle the WebDriver Exceptions. Log the specific
	 * Webdriver exception and the captured image
	 * </ul>
	 * 
	 * @version 1.0.0
	 */

	public static void webDriverExceptionHandler(ExtentTest test,
			WebDriverException e, String SETicket) {

		// Will log the specific WebDriver Exception
		if (e instanceof ElementNotVisibleException
				|| e instanceof NoSuchElementException
				|| e instanceof TimeoutException
				|| e instanceof StaleElementReferenceException
				|| e instanceof ImeActivationFailedException
				|| e instanceof ImeNotAvailableException
				|| e instanceof InvalidCookieDomainException
				|| e instanceof InvalidElementStateException
				|| e instanceof InvalidSelectorException
				|| e instanceof MoveTargetOutOfBoundsException
				|| e instanceof NoAlertPresentException
				|| e instanceof NoSuchFrameException
				|| e instanceof NoSuchWindowException
				|| e instanceof UnableToSetCookieException
				|| e instanceof UnexpectedTagNameException) {

			// Filename format of the captured image (SE ticket + date +
			// exception name)
			SimpleDateFormat sdf = new SimpleDateFormat("MMMdhm");
			String date = sdf.format(new java.util.Date());
			String filePath;
				
			filePath = ScreenTestCapture.TakeSnapShot(System.getProperty("user.dir")+
						"errorscreencaps/" + SETicket + date + e.getCause().getClass().getSimpleName() + ".png");

			// Log the exception error and the captured image
			e.getCause().getClass().getSimpleName();
			String lineNumber = Integer.toString(Thread.currentThread()
					.getStackTrace()[2].getLineNumber());
			String methodName = Thread.currentThread().getStackTrace()[2]
					.getMethodName();
			String lineNumber1 = Integer.toString(Thread.currentThread()
					.getStackTrace()[1].getLineNumber());
			String methodName1 = Thread.currentThread().getStackTrace()[1]
					.getMethodName();

			test.log(LogStatus.ERROR, StringQaHelper.append(
					"<span class='label failure'>", e.getCause().getClass()
							.getSimpleName(), " Due in method ", methodName,
					"at Line Number: ", lineNumber, "</span>"), StringQaHelper
					.append(e.getCause().getMessage().toString(),
							test.addScreenCapture(filePath)));
			test.log(LogStatus.ERROR, StringQaHelper.append(
					"<span class='label failure'>Exception Due in method",
					methodName1, "at Line Number: ", lineNumber1, "</span>"),
					"Additional Information");
			LOGGER.error(e.getCause().getStackTrace().toString());
		}
	}
}
