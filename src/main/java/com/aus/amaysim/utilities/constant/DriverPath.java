package com.aus.amaysim.utilities.constant;

/**
 * @author rguballo
 *
 */
public interface DriverPath {

	public static final String CHROME_DRIVER_PATH = System.getProperty("user.dir") + "\\driver\\chromedriver.exe";
}
