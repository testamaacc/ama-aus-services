package com.aus.amaysim.utilities.constant;

/**
 * @author rguballo
 *
 */
public interface DriverInfo {

	public static final String CHROME_DRIVER = "webdriver.chrome.driver";
	
}
