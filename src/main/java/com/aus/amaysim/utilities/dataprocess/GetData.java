package com.aus.amaysim.utilities.dataprocess;

import com.aus.amaysim.utilities.helper.ExtractExcelData;

/**
 * @author rguballo
 *
 */
public class GetData {

	private static GetData navElement;

	private GetData() {
	}

	public static GetData executeInstance() {
		if (navElement == null) {
			navElement = new GetData();
		}
		return navElement;
	}
	
	public String url(String variableName){
		
		String sUrlData = ExtractExcelData.getData("URLs.xlsx", "Sheet1", variableName);
		
		return sUrlData;
	}
	
	public String username(String username){
		
		String sUsername = ExtractExcelData.getData("Credentials.xlsx", "Sheet1", username);
		
		return sUsername;
	}
	
	public String password(String password){
		
		String sPassword = ExtractExcelData.getData("Credentials.xlsx", "Sheet1", password);
		
		return sPassword;
	}
	
	public String mobileNumber(String mobileNumber){
		
		String sMobileNumber = ExtractExcelData.getData("MobileNumbers.xlsx", "Sheet1", mobileNumber);
		
		return sMobileNumber;
	}
	
	public String phoneNumber(String phoneNumber){
		
		String sPhoneNumber = ExtractExcelData.getData("PersonalDetails.xlsx", "Sheet1", phoneNumber);
		
		return sPhoneNumber;
	}
	
	public String emailAddress(String emailAddress){
		
		String sEmailAddress = ExtractExcelData.getData("PersonalDetails.xlsx", "Sheet1", emailAddress);
		
		return sEmailAddress;
	}
	
	public String address(String address){
		
		String sAddress = ExtractExcelData.getData("PersonalDetails.xlsx", "Sheet1", address);
		
		return sAddress;
	}
	
	public String fullName(String fullName){
		
		String sFullName = ExtractExcelData.getData("PersonalDetails.xlsx", "Sheet1", fullName);
		
		return sFullName;
	}
	
	public String dateOfBirth(String dateOfBirth){
		
		String sDateOfBirth = ExtractExcelData.getData("PersonalDetails.xlsx", "Sheet1", dateOfBirth);
		
		return sDateOfBirth;
	}
	
	public String residential(String residential){
		
		String sResidential = ExtractExcelData.getData("PersonalDetails.xlsx", "Sheet1", residential);
		
		return sResidential;
	}
	
	public String delivery(String delivery){
		
		String sDelivery = ExtractExcelData.getData("PersonalDetails.xlsx", "Sheet1", delivery);
		
		return sDelivery;
	}
	
	public String search(String search){
		
		String sSearch = ExtractExcelData.getData("Search.xlsx", "Sheet1", search);
		
		return sSearch;
	}
}
