package com.aus.amaysim.suites;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.aus.amaysim.scripts.PaymentPage.PP_TC01;
import com.aus.amaysim.scripts.PersonalDetailsPage.PD_TC01;
import com.aus.amaysim.scripts.PlanPage.PLP_TC01;
import com.aus.amaysim.scripts.ServicePage.SP_TC01;

/**
 * @author rguballo
 *
 */
@RunWith(Suite.class)
@SuiteClasses({
	SP_TC01.class,
	PLP_TC01.class,
	PP_TC01.class,
	PD_TC01.class
	
})
public class Regression {

	//--> This class is used only as a holder for the above annotations
}
