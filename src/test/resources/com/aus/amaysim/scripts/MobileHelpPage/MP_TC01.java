package com.aus.amaysim.scripts.MobileHelpPage;

import org.junit.Test;

import com.aus.amaysim.main.LoginSession;
import com.aus.amaysim.openqa.pageobjects.HeaderBar;
import com.aus.amaysim.openqa.pageobjects.HomePage;
import com.aus.amaysim.openqa.pageobjects.MobileHelpPage;
import com.aus.amaysim.openqa.pageobjects.ServicePage;
import com.aus.amaysim.utilities.dataprocess.GetData;

/**
 * @author: rguballo
 * @description: Verify Payment And Payment method functionalities 
 *
 */
public class MP_TC01 extends LoginSession {
	
	@Test
	public void testMobileHelpPage() {
		HomePage.MenuContainer.MobileHelp.VERIFY_IF_DISPLAYED();
		HomePage.MenuContainer.MobileHelp.CLICK();
		
		MobileHelpPage.Header.findMobileHelp.VERIFY_IF_DISPLAYED();
		MobileHelpPage.Header.findMobileHelp.INSERT_TEXT(GetData.executeInstance().search("mobile_help_search"));
		MobileHelpPage.Header.autocomplete_SearchValue.VERIFY_IF_DISPLAYED();
		MobileHelpPage.Header.autocomplete_SearchValue.CLICK();
		
		MobileHelpPage.BlogHighlight.blogTitle.VERIFY_IF_DISPLAYED();
		MobileHelpPage.BlogHighlight.blogTitle.VERIFY_TEXT(GetData.executeInstance().search("mobile_help_search"));
		
		HeaderBar.TabSelection.myAccount.VERIFY_IF_ENABLED();
		HeaderBar.TabSelection.myAccount.CLICK();
		
		ServicePage.MobilePlan.serviceTileMobile.SELECT_TILE(GetData.executeInstance().mobileNumber("0468340754"));
		HomePage.MenuContainer.MobileHelp.VERIFY_IF_DISPLAYED();
	}	
}
