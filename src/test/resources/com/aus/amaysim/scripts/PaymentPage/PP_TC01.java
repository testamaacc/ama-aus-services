package com.aus.amaysim.scripts.PaymentPage;

import org.junit.Test;

import com.aus.amaysim.main.LoginSession;
import com.aus.amaysim.openqa.pageobjects.BrowserPage;
import com.aus.amaysim.openqa.pageobjects.HomePage;

/**
 * @author: rguballo
 * @description: Verify Payment And Payment method functionalities
 */
public class PP_TC01 extends LoginSession {

	@Test
	public void testPaymentPage() {
		
		HomePage.MenuContainer.Payments.VERIFY_IF_DISPLAYED();
		HomePage.MenuContainer.Payments.CLICK();
		
		BrowserPage.WAIT_FOR_COMPLETE_LOAD();
		HomePage.PaymentContent.payments.VERIFY_IF_DISPLAYED();
		HomePage.PaymentContent.paymentTransactionsTable.VERIFY_IF_DISPLAYED();
		HomePage.PaymentContent.paymentTransactionsTable_List.VERIFY_COUNT_LIST(6);
		
		HomePage.PaymentContent.showMore.VERIFY_IF_ENABLED();
		HomePage.PaymentContent.showMore.CLICK();
		BrowserPage.WAIT_FOR_COMPLETE_LOAD();
		
		HomePage.PaymentContent.paymentTransactionsTable_List.VERIFY_COUNT_LIST(12);
		
		HomePage.MenuContainer.PaymentMethod.VERIFY_IF_DISPLAYED();
		HomePage.MenuContainer.PaymentMethod.CLICK();
		
		BrowserPage.WAIT_FOR_COMPLETE_LOAD();
		HomePage.PaymentMethodContent.paymentMethod.VERIFY_IF_DISPLAYED();
		HomePage.PaymentMethodContent.simInfoTable.VERIFY_IF_DISPLAYED();
		
		HomePage.PaymentMethodContent.change.VERIFY_IF_ENABLED();
		HomePage.PaymentMethodContent.change.ACTION_CLICK();
		BrowserPage.WAIT_FOR_COMPLETE_LOAD();
		
		HomePage.PaymentMethodContent.changeTo_NewPaymentMethod.VERIFY_IF_DISPLAYED();
		HomePage.PaymentMethodContent.changeTo_NewPaymentMethod.SELECT_OPTION_BY_TEXT("BPAY / Vouchers");
		HomePage.PaymentMethodContent.cancel_BPMethod.VERIFY_IF_ENABLED();
		HomePage.PaymentMethodContent.cancel_BPMethod.CLICK();
		BrowserPage.WAIT_FOR_COMPLETE_LOAD();
		
		HomePage.PaymentMethodContent.change.VERIFY_IF_DISPLAYED();
		HomePage.PaymentMethodContent.change.ACTION_CLICK();
		BrowserPage.WAIT_FOR_COMPLETE_LOAD();
		
		HomePage.PaymentMethodContent.changeTo_NewPaymentMethod.VERIFY_IF_DISPLAYED();
		HomePage.PaymentMethodContent.changeTo_NewPaymentMethod.SELECT_OPTION_BY_TEXT("BPAY / Vouchers");
		HomePage.PaymentMethodContent.save_BPMethod.VERIFY_IF_ENABLED();
		HomePage.PaymentMethodContent.save_BPMethod.CLICK();
		BrowserPage.WAIT_FOR_COMPLETE_LOAD();
		
		HomePage.SuccessfulModal.success_Header.VERIFY_IF_DISPLAYED();
		HomePage.SuccessfulModal.closeRevealModal.CLICK();
	}
}
