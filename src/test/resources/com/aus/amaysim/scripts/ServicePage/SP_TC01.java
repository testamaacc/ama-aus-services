package com.aus.amaysim.scripts.ServicePage;

import org.junit.Test;

import com.aus.amaysim.main.LoginSession;
import com.aus.amaysim.openqa.pageobjects.HomePage;
import com.aus.amaysim.openqa.pageobjects.ServicePage;
import com.aus.amaysim.utilities.dataprocess.GetData;

/**
 * @author: rguballo
 * @description: @description: Verify Services Page Functionalities
 *
 */
public class SP_TC01 extends LoginSession {

	@Test
	public void testServicePage() {
		HomePage.MenuContainer.myDashboard.VERIFY_IF_DISPLAYED();
		HomePage.MenuContainer.myDashboard.CLICK();
		
		ServicePage.MobilePlan.mobilePlan_Header.VERIFY_IF_DISPLAYED();
		ServicePage.MobilePlan.newMobilePlan.VERIFY_IF_DISPLAYED();
		ServicePage.EnergyPlan.switchToday.VERIFY_IF_DISPLAYED();
		ServicePage.MobilePlan.serviceTileMobile.SELECT_TILE(GetData.executeInstance().mobileNumber("0468431517"));
		
		HomePage.MenuContainer.service_Primary_Info.VERIFY_IF_DISPLAYED();
		HomePage.MenuContainer.service_Primary_Info.VERIFY_TEXT(GetData.executeInstance().mobileNumber("0468431517"));
		
		
		HomePage.MenuContainer.myDashboard.VERIFY_IF_DISPLAYED();
		HomePage.MenuContainer.myDashboard.CLICK();
		
		ServicePage.MobilePlan.serviceTileMobile.SELECT_TILE(GetData.executeInstance().mobileNumber("0468318671"));
		
		HomePage.MenuContainer.service_Primary_Info.VERIFY_IF_DISPLAYED();
		HomePage.MenuContainer.service_Primary_Info.VERIFY_TEXT(GetData.executeInstance().mobileNumber("0468318671"));
		
	}
}
