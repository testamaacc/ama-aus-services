### Important reminders:

- Use Eclipse IDE for easy access of codes
- Bitbucket repository url: https://bitbucket.org/testamaacc/ama-aus-services/overview

- Import using 'Project from Git'
- Clone repository: https://bitbucket.org/testamaacc/ama-aus-services.git

- Use Package Explorer and organize it in Hierarchical package presentation for greater view

- Test report will be automatically generated after running the suite. 
You can see the report using the [reports] folder. After the suite execution kindly refresh the project so that reports will appear. 

- For test suite execution, use Junit and run Regression.java class in this directory: src\test\resources\com\aus\amaysim\suites\Regression.java